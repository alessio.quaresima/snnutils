include("structs.jl")
include("parameters.jl")
include("functions.jl")

export duarte_types, pv_only, sst_only, duartemorrison_2017, lkd_2014
