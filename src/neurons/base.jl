using Random
using Distributions
using Printf

include("neurons.jl")
include("equations.jl")
